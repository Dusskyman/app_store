import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:layout_building_appstore/models/restorant.dart';

abstract class HttpRepo {
  static Future<Restorant> getData() async {
    String url =
        'https://random-data-api.com/api/restaurant/random_restaurant/';
    return await http
        .get(url)
        .then((value) => Restorant.fromMap(json.decode(value.body)));
  }
}
