import 'package:flutter/widgets.dart';
import 'package:layout_building_appstore/models/restorant.dart';

class RestorantProvider with ChangeNotifier {
  final List<Restorant> _listRestorant = [];

  List<Restorant> get listRest => _listRestorant;

  void listAdd(Restorant restorant) {
    _listRestorant.add(restorant);
    notifyListeners();
  }
}
