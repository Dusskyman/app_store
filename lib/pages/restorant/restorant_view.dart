import 'package:flutter/material.dart';
import 'package:layout_building_appstore/pages/restorant/widgets/restorant_card.dart';
import 'package:layout_building_appstore/pages/restorant/widgets/restorant_detail_page.dart';
import 'package:layout_building_appstore/providers/restorant_provider.dart';
import 'package:provider/provider.dart';

class RestorantView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var querrySize = MediaQuery.of(context).size;
    var restorantData = Provider.of<RestorantProvider>(context);
    return SafeArea(
      child: Container(
        alignment: Alignment.center,
        width: double.infinity,
        height: querrySize.height * 0.85,
        child: ListView.builder(
          itemCount: restorantData.listRest.length,
          itemBuilder: (context, index) => InkWell(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    RestorantDetailPage(restorantData.listRest[index]),
              ),
            ),
            child: RestorantCard(
              restorantData.listRest[index],
              callback: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        RestorantDetailPage(restorantData.listRest[index]),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
