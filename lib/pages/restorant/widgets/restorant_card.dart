import 'package:flutter/material.dart';
import 'package:layout_building_appstore/models/restorant.dart';

class RestorantCard extends StatefulWidget {
  final Restorant restorant;
  final Function() callback;
  RestorantCard(this.restorant, {this.callback});

  @override
  _RestorantCardState createState() => _RestorantCardState();
}

class _RestorantCardState extends State<RestorantCard> {
  @override
  Widget build(BuildContext context) {
    var querrySize = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10, bottom: 20),
      child: Center(
        child: SizedBox(
          height: querrySize.width * 0.9,
          width: querrySize.width * 0.9,
          child: Hero(
            tag: widget.restorant.id,
            flightShuttleBuilder:
                (flightContext, animation, direction, fromcontext, toContext) {
              final Hero toHero = toContext.widget;
              // Change push and pop animation.
              return direction == HeroFlightDirection.push
                  ? ScaleTransition(
                      scale: animation.drive(
                        Tween<double>(
                          begin: 0.75,
                          end: 1.02,
                        ).chain(
                          CurveTween(
                              curve:
                                  Interval(0.4, 1.0, curve: Curves.easeInOut)),
                        ),
                      ),
                      child: toHero.child,
                    )
                  : SizeTransition(
                      sizeFactor: animation,
                      child: toHero.child,
                    );
            },
            child: Stack(
              children: [
                SizedBox(
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(25),
                      child: Image.network(widget.restorant.logoUrl)),
                ),
                Positioned(
                  bottom: 23,
                  left: 10,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        '\$${widget.restorant.id}.99',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.blue[600],
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        width: 100,
                        height: 50,
                        child: FittedBox(
                          child: Text(
                            '${widget.restorant.name}',
                            style: TextStyle(fontSize: 30, color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Icon(Icons.restaurant),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
