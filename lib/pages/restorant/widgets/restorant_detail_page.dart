import 'package:flutter/material.dart';
import 'package:layout_building_appstore/models/restorant.dart';

class RestorantDetailPage extends StatefulWidget {
  final Restorant restorant;
  RestorantDetailPage(this.restorant);

  @override
  _RestorantDetailPageState createState() => _RestorantDetailPageState();
}

class _RestorantDetailPageState extends State<RestorantDetailPage> {
  ScrollController scrollController;
  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: widget.restorant.id,
      flightShuttleBuilder:
          (flightContext, animation, direction, fromcontext, toContext) {
        final Hero toHero = toContext.widget;
        return direction == HeroFlightDirection.push
            ? ScaleTransition(
                scale: animation.drive(
                  Tween<double>(
                    begin: 0.75,
                    end: 1.02,
                  ).chain(
                    CurveTween(
                      curve: Interval(0.7, 1.0, curve: Curves.easeInOut),
                    ),
                  ),
                ),
                child: toHero.child,
              )
            : ScaleTransition(
                scale: animation.drive(
                  Tween<double>(
                    begin: 0.75,
                    end: 1.02,
                  ).chain(
                    CurveTween(
                      curve: Interval(0.7, 1.0, curve: Curves.easeInOut),
                    ),
                  ),
                ),
                child: toHero.child,
              );
      },
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Stack(
                        children: [
                          SizedBox(
                            child: Image.network(
                              widget.restorant.logoUrl,
                            ),
                          ),
                          Positioned(
                            bottom: 23,
                            left: 10,
                            child: Container(
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: Text(
                                  '\$${widget.restorant.id}.99',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blue[800],
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 10,
                            right: 10,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  width: 100,
                                  height: 50,
                                  child: FittedBox(
                                    child: Text(
                                      '${widget.restorant.name}',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Icon(Icons.restaurant),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            left: 10,
                            top: 10,
                            child: IconButton(
                              icon: Icon(
                                Icons.cancel,
                              ),
                              color: Colors.white70,
                              iconSize: 30,
                              onPressed: () => Navigator.pop(context),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      child: Column(
                        children: [
                          Divider(
                            thickness: 3,
                          ),
                          Text(
                            'Review',
                            style: TextStyle(fontSize: 25),
                          ),
                          Text(
                            widget.restorant.review,
                            style: TextStyle(fontSize: 20),
                          ),
                          Divider(
                            thickness: 3,
                          ),
                          Text(
                            'Description',
                            style: TextStyle(fontSize: 25),
                          ),
                          Text(
                            widget.restorant.description,
                            style: TextStyle(fontSize: 20),
                          ),
                          Divider(
                            thickness: 3,
                          ),
                          Text(
                            'Contacts:',
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            "\nAddress: \n${widget.restorant.address} \nPhone: \n${widget.restorant.phone}",
                            style: TextStyle(fontSize: 20, color: Colors.blue),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.grey[200],
                      width: double.infinity,
                      height: 200,
                      child: Column(
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Icon(
                              Icons.restaurant,
                              size: 40,
                            ),
                          ),
                          SizedBox(
                            height: 80,
                            width: 200,
                            child: FittedBox(
                              child: Text(
                                '${widget.restorant.name}',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: Colors.blue[600],
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 10.0),
                              child: Text(
                                '\$${widget.restorant.id}.99',
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              AnimatedPositioned(
                curve: Curves.easeInBack,
                duration: Duration(milliseconds: 300),
                bottom: scrollController.hasClients &&
                        (scrollController.position.pixels > 350 &&
                            scrollController.position.pixels <
                                scrollController.position.maxScrollExtent - 200)
                    ? 20
                    : -100,
                left: 20,
                right: 20,
                child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15)),
                  height: 75,
                  child: SingleChildScrollView(
                    physics: NeverScrollableScrollPhysics(),
                    child: Row(
                      children: [
                        Container(
                          height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Text(
                              '\$${widget.restorant.id}.99',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.blue[600],
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          child: SizedBox(),
                        ),
                        SizedBox(
                          width: 100,
                          height: 50,
                          child: FittedBox(
                            child: Text(
                              '${widget.restorant.name}',
                              style:
                                  TextStyle(fontSize: 30, color: Colors.black),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Icon(Icons.restaurant),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
