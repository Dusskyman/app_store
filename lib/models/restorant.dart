class Restorant {
  final String id;
  final String name;
  final String type;
  final String description;
  final String review;
  final String phone;
  final String address;
  final String logoUrl;

  Restorant(
      {this.id,
      this.name,
      this.type,
      this.description,
      this.review,
      this.phone,
      this.address,
      this.logoUrl});

  Restorant.fromMap(Map map)
      : this.id = map['id'].toString(),
        this.name = map['name'],
        this.type = map['type'],
        this.description = map['description'],
        this.review = map['review'],
        this.phone = map['phone_number'],
        this.address = map['address'],
        this.logoUrl = map['logo'];
}
