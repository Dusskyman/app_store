import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:layout_building_appstore/pages/restorant/restorant_view.dart';
import 'package:layout_building_appstore/providers/restorant_provider.dart';
import 'package:layout_building_appstore/repositories/http_repository.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
    );
    return ChangeNotifierProvider<RestorantProvider>(
      create: (context) => RestorantProvider(),
      builder: (context, child) => MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var restorantData = Provider.of<RestorantProvider>(context);
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        RestorantView(),
        IconButton(
            icon: Icon(Icons.add),
            onPressed: () async {
              await HttpRepo.getData()
                  .then((value) => restorantData.listAdd(value));
            })
      ],
    ));
  }
}
